# Modlist

A small program that utilizes the `golang.org/x/tools/go/packages` tooling to print out
the utilized go packages.

```shell
$ go build -o modlist main.go
$ ./modlist -h
Usage of ./modlist:
  -includeTest
        controls whether test only packages are included (default true)
$ ./modlist -includeTest=false
Name: gitlab.com/hacks4oats/go-modlist Version: 1.18
Name: log Version:
# ...
```

This is a proof of concept and as a result has a lot of missed edge cases.
