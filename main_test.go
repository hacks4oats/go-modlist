package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSum(t *testing.T) {
	expect := 2
	got := 1 + 1
	assert.Equal(t, expect, got)
}
